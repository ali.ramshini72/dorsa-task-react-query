import {Overlay, Drawer, Content} from './styles'
import {useRef} from "react";
import useOutsideClick from "../hooks/utils/useOutsideClick";

interface Props {
    isOpen: boolean,
    onClose: () => void,
    children: React.ReactNode
}

export default function AppDrawer(props: Props) {
    const {isOpen, children , onClose} = props;
    const ref: any = useRef("drop-down-btn")
    useOutsideClick(ref, () => {
        onClose()
    })
    return (
        <>
            <Overlay isOpen={isOpen}/>
            <Drawer isOpen={isOpen}>
                <Content ref={ref}>
                    {children}
                </Content>
            </Drawer>
        </>
    );
}
