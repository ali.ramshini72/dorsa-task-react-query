// Drawer component
import styled from "styled-components";

export const Drawer = styled.div<{ isOpen: boolean }>`
  position: fixed;
  bottom: 0;
  left : 0 ;
  right : 0 ;
  height: fit-content;
  box-shadow: 2px 0 6px rgba(0, 0, 0, 0.3);
  transform: translateY(${props => (props.isOpen ? '0' : '100%')});
  transition: transform 0.3s ease-in-out;
`;

export const Content = styled.div`
    background-color: white;
    width : 100%;
 max-width : 480px;
  margin: 0 auto;
  border-top-left-radius : 1rem ;
  border-top-right-radius : 1rem ;
  padding: .5rem;
    
`

// Overlay component
export const Overlay = styled.div<{ isOpen: boolean }>`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
  opacity: ${props => (props.isOpen ? '1' : '0')};
  pointer-events: ${props => (props.isOpen ? 'auto' : 'none')};
  transition: opacity 0.3s ease-in-out;
`;
