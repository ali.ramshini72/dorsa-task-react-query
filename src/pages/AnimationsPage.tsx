import useAnimations from "../hooks/useAnimations";
import {PageContiner, PageHeader, PageNav, PageTitle, Row, Title} from "./styles";
import arrowIcon from '../icons/arrow_right.svg'
import AnimationItem from "../components/animation/AnimationItem";
import SelectSortBy from "../components/animation/SelectSortBy";


const sortByList = ["newest", "rate", "view"]

export default function AnimationsPage() {
    const {
        ref,
        status,
        error,
        data,
        isFetchingNextPage,
        hasNextPage,
        onChangeSort
    } = useAnimations()
    if (status === "loading") return <PageContiner><h1>Loading...</h1></PageContiner>
    if (status === "error") return <PageContiner><h1>{JSON.stringify(error)}</h1></PageContiner>
    return (
        <PageContiner>
            <PageHeader>
                <PageNav>
                    <span>
                         <img src={arrowIcon} alt={'react-query'}/>
                    </span>
                    <span>
                        بازگشت
                    </span>
                </PageNav>
                <PageTitle>
                    <Title>
                        <h4>
                            چیارو ببیینه؟
                        </h4>
                        <p>مناسب برای 3 تا 7</p>
                    </Title>
                    <SelectSortBy onChangeSort={onChangeSort} sortByList={sortByList}/>
                </PageTitle>
            </PageHeader>
            <Row>
                {data?.pages
                    .flatMap(data => data.data)
                    .map((anim) => (
                        <AnimationItem
                            key={anim.id}
                            imgUrl={anim.reviewsThumbnailUrl}
                            title={anim.reviewsTitle}
                            rate={anim.reviewsRate}
                        />
                    ))}
            </Row>
            <div ref={ref} style={{height: '50px'}}>
                <p>{hasNextPage ? "" : 'end'}</p>
                <p>{isFetchingNextPage ? "loading ... " : ""}</p>
            </div>
        </PageContiner>
    )
}
