import styled from 'styled-components';


export const PageContiner = styled.div`
 width : 100%;
 max-width : 480px;
  margin: 0 auto;
  background-color : white;
  direction : rtl;
  padding : .25rem .5rem ;
  min-height : 100vh
`
export const PageHeader = styled.header`
  position : sticky;
  top : 0;
  min-width : 100% ;
  background : white;
`
export const PageNav = styled.nav`
padding : .5rem 0 ;
display : inline-flex;
  gap : .5rem; 
  align-items: center;
  color : #858c96;
`
export const PageTitle = styled.div`
    display : flex ;
    align-items: center;
    width : 100%
`
export const Title = styled.div`
    text-align : right;
    flex-basis : 50%;
    h4 {
     margin : .5rem;
     font-size : 1.5rem
    }
    p {
       margin : .5rem;
       color : #858c96
    }
`
export const Sort = styled.div`
    flex-basis : 50%;
    display : inline-flex ;
    gap : .25rem;
    color : #858c96 ;
    justify-content : end ;
`

export const Row = styled.div`
    display : flex ;
    flex-wrap : wrap ;
`
