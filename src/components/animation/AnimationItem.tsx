import {Animation , AnimationImage, AnimationTitle} from "./styles";
import starIcon from '../../icons/vuesax_linear_star.svg'

interface Props {
    imgUrl : string ,
    title : string ,
    rate : number
}

export default function AnimationItem(props : Props) {
    const {imgUrl , title , rate} = props;
    return (
        <Animation>
            <AnimationImage src={imgUrl} />
            <AnimationTitle>{title}</AnimationTitle>
            <section>
                <img src={starIcon} alt={'star'} />
                <span>
                    {rate}
                </span>
            </section>
        </Animation>
    )
}
