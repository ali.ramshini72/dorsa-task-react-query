import sortIcon from "../../icons/sort.svg";
import {Sort} from "../../pages/styles";
import {useState} from "react";
import AppDrawer from '../../baseComponents/AppDrawer'

interface Props {
    sortByList: string[];
    onChangeSort: (value: string) => void
}


export default function SelectSortBy(props: Props) {
    const {sortByList, onChangeSort} = props
    const [isOpen, setIsOpen] = useState(false);

    const onItem = (value: string) => {
        setIsOpen(false)
        onChangeSort(value)
    }
    return (
        <Sort>
            <img className={'pointer'} src={sortIcon} alt={'sort'} onClick={() => setIsOpen(true)}/>
            <span className={"pointer"} onClick={() => setIsOpen(true)}>
                            مرتب سازی
                        </span>

            <AppDrawer isOpen={isOpen} onClose={() => setIsOpen(false)}>
                <h5>
                    مرتب سازی
                </h5>

                {sortByList.map(item => <p className={'pointer'} key={item} onClick={() => onItem(item)}>{item}</p>)}


            </AppDrawer>
        </Sort>
    )
}
