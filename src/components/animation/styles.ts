import styled from 'styled-components';


export const Animation = styled.div`
 margin-bottom : .5rem;
 padding : .25rem;
 flex-basis : calc(50%)
`
export const AnimationImage = styled.img`
 width : 100%;
 border-radius : 1rem ;
 height : 256px ;
 margin-bottom : .25rem
`
export const AnimationTitle = styled.h5`
 margin :  .5rem 0 ;
`

